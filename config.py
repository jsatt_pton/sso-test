from environs import Env

env = Env()
env.read_env()

ENV = env.str('FLASK_ENV', 'production')
DEBUG = env.bool('FLASK_DEBUG', False)
TESTING = env.bool('TESTING', False)
SERVER_NAME = 'localhost:8000'

USE_GRAPHIQL = env.bool('USE_GRAPHIQL', False)

AUTH0_CLIENT_ID = env.str('AUTH0_CLIENT_ID', '')
AUTH0_SIGNING_CERT = env.str('AUTH0_SIGNING_CERT', '')
