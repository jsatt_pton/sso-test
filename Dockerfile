FROM python:3.10-slim-buster

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1
ENV POETRY_VIRTUALENVS_CREATE=0

RUN adduser --uid 1000 --gecos --quiet --disabled-password flask

RUN mkdir -p /usr/src/app \
    && chown flask.flask /usr/src/app
WORKDIR /usr/src/app

COPY --chown=flask pyproject.toml poetry.lock ./

ARG POETRY_ARGS=
RUN set -eux \
    && apt update \
    && apt upgrade -y \
    && apt install -y curl libffi-dev gpg build-essential \
    && pip3 install poetry \
    && poetry install --no-root --no-interaction $POETRY_ARGS \
    && apt remove -y build-essential \
    && apt autoremove -y \
    && rm -rf /root/.cache/pip /root/.cache/pypoetry /var/cache/apt

COPY --chown=flask . .

USER flask

CMD ["/usr/local/bin/gunicorn", "app:app", "--bind", "0.0.0.0:8000"]
