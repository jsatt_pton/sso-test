document.addEventListener('DOMContentLoaded', async () => {
  const results = document.getElementById('results');
  results.innerText = 'Loading....';
  
  const logout_btn = document.getElementById('logout')
  logout_btn.addEventListener('click', async () => {
    await auth0.logout();
  });

  console.log(window.location.href);
  const auth0 = await window.createAuth0Client({
    //domain: 'jsatt.us.auth0.com',
    //client_id: '5t1skttOqW6CyZJ5PLinjVHc9b7pUhjk',
    //connection: 'Username-Password-Authentication',
    domain: 'auth1-dev.onepeloton.com',
    client_id: 'RqofCrmM5vG1wC4s7lzQZm3INl8dX1iJ',
    connection: 'Peloton-OKTA-prod',
    redirect_uri: window.location.href,
  });

  let params = new URLSearchParams(window.location.search);
  if(params.has('code')) {
    try {
      let redirect_result = await auth0.handleRedirectCallback();
    } catch {}
  }
  //const accessToken = await auth0.getTokenSilently();
  if(await auth0.isAuthenticated()){
    let claims = await auth0.getIdTokenClaims();
    let jwt = claims.__raw;
    retrieve(jwt).then((res) => {results.innerText = res});
  } else {
    await auth0.loginWithRedirect();
  }
});

var retrieve = async (jwt) => { 
  if (jwt){
    let res = await fetch('/graphql', {
      'method': 'POST',
      'headers': {'Content-Type': 'application/json'},
      'body': JSON.stringify({
        'operationName': 'Enroll',
        'variables': {'jwt': jwt},
        'query': `
          mutation Enroll($jwt: String){
            enrollFromJwt(jwtToken: $jwt){
              ok
              person{
                name
                emailAddress
                accessToken
              }
            }
          }
        `
      }),
    });
    return res.text();
  }else{
    return 'no JWT provided';
  }
}

export {}
