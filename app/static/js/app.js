document.addEventListener('DOMContentLoaded', function(){
  results = document.getElementById('results');
  results.innerText = 'Loading....';

  jwt = getJWT();
  retrieve(jwt).then((res) => {results.innerText = res});
}, false);


var getJWT = () => {
  let query = new URLSearchParams(window.location.search);
  let jwt = query.get('jwt');
  return jwt
}


var retrieve = (jwt) => { 
  if (jwt){
    return fetch('/graphql', {
      'method': 'POST',
      'headers': {'Content-Type': 'application/json'},
      'body': JSON.stringify({
        'operationName': 'Enroll',
        'variables': {'jwt': jwt},
        'query': `
          mutation Enroll($jwt: String){
            enrollFromJwt(jwtToken: $jwt){
              ok
              person{
                name
                emailAddress
                accessToken
              }
            }
          }
        `
      }),
    })
    .then((res) => res.text())
  }else{
    return Promise.resolve('no JWT provided');
  }
}
