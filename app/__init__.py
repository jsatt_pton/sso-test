from flask import Flask, render_template
from flask_graphql import GraphQLView

from .commands import shell_plus
from .schema import schema

app = Flask(__name__)
app.config.from_object('config')
app.cli.add_command(shell_plus, 'shell-plus')

app.add_url_rule(
    '/graphql',
    view_func=GraphQLView.as_view('graphql', schema=schema, graphiql=app.config['USE_GRAPHIQL']),
)


@app.route('/benefits/auth')
def home():
    return render_template('home.html')
