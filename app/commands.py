import sys
from textwrap import dedent

import click
from flask.cli import with_appcontext


@click.command(context_settings=dict(ignore_unknown_options=True))
@click.argument('ipython_args', nargs=-1, type=click.UNPROCESSED)
@with_appcontext
def shell_plus(ipython_args):
    """Runs a shell in the app context.

    Cloned from: https://github.com/ei-grad/flask-shell-ipython
    Runs an interactive Python shell in the context of a given
    Flask application. The application will populate the default
    namespace of this shell according to it's configuration.
    This is useful for executing small snippets of management code
    without having to manually configuring the application.
    """
    import IPython  # pylint: disable=import-outside-toplevel
    from flask.globals import _app_ctx_stack  # pylint: disable=import-outside-toplevel
    from IPython.terminal.ipapp import load_default_config  # pylint: disable=import-outside-toplevel
    from traitlets.config.loader import Config  # pylint: disable=import-outside-toplevel

    app = _app_ctx_stack.top.app

    if 'IPYTHON_CONFIG' in app.config:
        config = Config(app.config['IPYTHON_CONFIG'])
    else:
        config = load_default_config()

    config.TerminalInteractiveShell.banner1 = dedent(
        f'''Python {sys.version} on {sys.platform}
        IPython: {IPython.__version__}
        App: {app.import_name} [{app.env}]
        Instance: {app.instance_path}''')
    IPython.start_ipython(
        argv=ipython_args,
        user_ns=app.make_shell_context(),
        config=config,
    )
