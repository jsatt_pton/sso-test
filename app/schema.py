import logging
from hashlib import sha256

import graphene
import jwt
from jwt.exceptions import PyJWTError
from cryptography.hazmat.backends import default_backend
from cryptography.x509 import load_pem_x509_certificate
from flask import current_app


class Person(graphene.ObjectType):
    name = graphene.String()
    email_address = graphene.String()
    access_token = graphene.String()


class EnrollFromJWT(graphene.Mutation):
    class Arguments:
        jwt_token = graphene.String()

    ok = graphene.Boolean()
    person = graphene.Field(lambda: Person)

    @staticmethod
    def mutate(root, info, jwt_token):
        if jwt_token:
            pub_key = load_pem_x509_certificate(
                current_app.config['AUTH0_SIGNING_CERT'].encode(), default_backend()).public_key()
            try:
                user_data = jwt.decode(
                    jwt_token, pub_key, algorithms=['RS256'],
                    # options={"verify_aud": False})
                    audience=current_app.config['AUTH0_CLIENT_ID'])
            except PyJWTError as exc:
                current_app.logger.exception('Unable to decode JWT', exc_info=exc)
                person = None
                ok = False
            else:
                # print(user_data)
                access_token = sha256(user_data['email'].encode()).hexdigest()
                person = Person(
                    name=user_data['name'],
                    email_address=user_data['email'],
                    access_token=access_token)
                ok = True
        return EnrollFromJWT(person=person, ok=ok)


class Mutations(graphene.ObjectType):
    enroll_from_jwt = EnrollFromJWT.Field()


class Query(graphene.ObjectType):
    person = graphene.Field(Person)


schema = graphene.Schema(query=Query, mutation=Mutations)
